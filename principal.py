import pygame
from libreria import *
from libreria import BLANCO, Cart_Pant, Rotacion

if __name__ == '__main__':
    pygame.init()
    pantalla = pygame.display.set_mode([ANCHO, ALTO])

    centro =[400,500]
    a=[90,0]
    b=[60,0]
    c=[30,0]
    d=[30,0]
    e=[60,0]

    a_r=Rotacion(a,30)
    a_rc=Cart_Pant(a_r, centro)
    
    b_r=Rotacion(b,90)
    b_rc=Cart_Pant(b_r, a_rc)

    c_r=Rotacion(c,210)
    c_rc=Cart_Pant(c_r, b_rc)

    d_r=Rotacion(d,270)
    d_rc=Cart_Pant(d_r, c_rc)

    e_r=Rotacion(e,210)
    e_rc=Cart_Pant(e_r, d_rc)
    ls_v1=[centro, a_rc, b_rc, c_rc, d_rc, e_rc]
    '''pygame.draw.circle(pantalla, BLANCO, centro, 2)
    pygame.draw.circle(pantalla, BLANCO, a_rc, 2)
    pygame.draw.circle(pantalla, BLANCO, b_rc, 2)
    pygame.draw.circle(pantalla, BLANCO, c_rc, 2)
    pygame.draw.circle(pantalla, BLANCO, d_rc, 2)
    pygame.draw.circle(pantalla, BLANCO, e_rc, 2)'''
    pygame.draw.polygon(pantalla, BLANCO, ls_v1)
    pygame.display.flip()

    fin = False
    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True

        
    pygame.quit()
    

    print('Fin del programa')
